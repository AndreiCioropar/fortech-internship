class User {
    constructor(name) {
        this.name = name;
    }
    toString() {
        return this.name.toString();
    }
}

class Issue {

    constructor(type, name, userId, assignee, description, sprintId, id) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.createdBy = userId;
        this.assignee = assignee;
        this.description = description;
        this.sprint = sprintId;
        this.tasks = [];
        this.comments = {};
        this.commentId = 1;
        this.status = "New";
        this.createdAt = new Date();
        this.updatedAt = new Date();
        this.canHaveSubTask = type == "bug" || type == "feature" ? true : false;
    }

    toString() {
        return type + " " + name + " " + createdBy + " " + assignee + " " + description + " " + status + " " + tasks + " " + comments + " " + commentId + " " + updatedAt + " " + createdAt;
    }
    addComment(name) {
        var comm = new Comments(name);
        this.comments[this.generateCommentId()] = comm;
    }
    generateCommentId() {
        this.commentId = commentId + 2;
        return commentId;
    }
}

class Project {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.sprints = [];
        this.sprintId = 0;
    }
    addSprint(sprint) {
        sprint.id = this.generateSprintId();
        this.sprints[sprint.id] = sprint;
    }
    generateSprintId() {
        this.sprintId = sprintId + 1;
        return sprintId;
    }
}

class Sprint {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.issues = {};
        this.issueId = 0;
    }
    generateIssueId() {
        this.issueId = issueId + 1;
        return issueId;
    }
    addIssue(issue) {
        issue.id = this.utilities.generateId(this.lastId);
        this.issues[issue.id] = issue;
        this.lastId = issue.id;
    }
    toString() {
        s = name;
        for (i in issues) {
            s = s + i + issues[i].toString();
        }
        s = s + issueId;
    }
}

class Comments {
    constructor(name) {
        this.name = name;
    }
}



// USER REPOSITORY 

class userRepo {
    constructor(utilities) {
        this.users = {};
        this.lastId = 0;
        this.utilities = utilities;
    }
    addUser(name) {
        var user = new User(name);
        user.id = this.utilities.generateId(this.lastId);
        this.users[user.id] = user;
        this.lastId = user.id;
    }
}
class projectRepo {
    constructor(utilities) {
        this.project = [];
        this.lastId = 0;
        this.utilities = utilities;
    }
    createProject(projectId, name) {
        var newProjectId = this.utilities.generateId(this.lastId);
    }
    getProjectById(projectId) {
        return this.project.filter(function (project) { return project.id == projectId })[0];
    }
}

class sprintRepo {
    constructor(utilities) {
        this.sprints = [];
        this.utilities = utilities;
        this.lastid = 0;
    }
    createSprint(sprintId, name) {
        var newProjectId = this.utilities.generateId(this.lastId);
    }
    getSprintsByProjectId(projectId) {
        var projectsSprints = [];
        projectId.forEach(function (sprintId) {
            var findSprintInList = sprints.filter(function (sprint) { return sprint.id == sprintId; })[0];
            if (findSprintInList != undefined) {
                projectsSprints.push(findSprintInList);
            }
        });

        return projectsSprints;
    }
}

class issueRepo {
    constructor(utilities) {
        this.issues = [];
        this.lastId = 0;
        this.utilities = utilities;
    }

    createIssue(userId, type, name, assignee, description, sprintId, issueParentId) {
        var newId = this.utilities.generateId(this.lastId);
        // Update last id
        this.lastId = newId;
        var newIssue = new Issue(type, name, userId, assignee, description, sprintId, newId);
        this.issues.push(newIssue);
        //Update Parent Issue
        this.uptadeParentIssue(issueParentId, newId);
    }

    getIssueById(issueId) {
        var issueArray = this.issues.filter(function (issue) { return issue.id == issueId; })
        if (issueArray.length == 0)
            return null;
        return issueArray[0];
    }

    getIssuesBySprintId(sprintId) {
        return this.issues.filter(function (issue) { return issue.sprint == sprintId; });
    }

    updateIssue(updatedIssue) {
        var existingIssue = this.getIssueById(updatedIssue.id);

        existingIssue.type = updatedIssue.type;
        existingIssue.sprintId = updatedIssue.sprintId;
        //TODO: Add rest of properties

        existingIssue.updatedAt = new Date();
    }

    uptadeParentIssue(issueParentId, newlyAddedTaskId) {
        if (issueParentId != null && issueParentId != undefined) {
            var existingIssue = this.getIssueById(issueParentId);
            // Only bugs and feature can have subtasks
            if (existingIssue.canHaveSubTask)
                existingIssue.tasks.push(newlyAddedTaskId);
        }
    }
}

class utilities {
    constructor() {
    }
    generateId(id) {
        return id + 1;
    }
}
var util = new utilities();

var ur = new userRepo(util);
ur.addUser("Andrei");
ur.addUser("Dorin");

for (i in ur.users) {
    console.log(i, ur.users[i].toString());
}


showoverall(util);

function addNewIssue(userId, issueRep) {
    issueRep.createIssue(1, "bug", "test", "dev1", "description", 1, null);
    issueRep.createIssue(2, "feature", "test", "dev1", "description", 2, null);
    issueRep.createIssue(1, "task", "test", "dev1", "description", 1, 2);

    issueRep.issues.forEach(function (issue) {
        console.log(issue);
    });
} 

function addNewProject(userId, projectRep) {
    projectRep.createProject(1, "Project");

    projectRep.project.forEach(function (project) {
        console.log(project);
    });
}

function addNewSprint(userId, sprintRep) {
    sprintRep.createSprint(1, "Sprint");
    sprintRep.createSprint(2, "Sprint2");

    sprintRep.sprint.forEach(function (sprint) {
        console.log(sprint);
    })
}

function showoverall(utilities) {

    var issueR = new issueRepo(utilities);
    var projectR = new projectRepo(utilities);
    // AddNewProject
    // AddNEwSprint

    addNewIssue(1, issueR);
    addNewProject(1, projectR);
    
    var sprintIssues = issueR.getIssuesBySprintId(1);
    sprintIssues.forEach(function (issue) {
        console.log(issue);
    });

    var projectss = projectR.getProjectById(1);
    projectss.forEach(function (project) {
        console.log(project);
    });
}

function getSprints(ids) {
    var projectsSprints = [];
    ids.forEach(function (sprintId) {
        var findSprintInList = sprints.filter(function (sprint) { return sprint.id == sprintId; })[0];
        if (findSprintInList != undefined) {
            projectsSprints.push(findSprintInList);
        }
    });

    return projectsSprints;
}

function getIssues(sprintId) {
    var issues = issues.filter(function (issue) { return issue.sprintId == sprintId; });
    return issues;
}

function getIssues(sprintId, issueType) {
    var issues = issues.filter(function (issue) { return issue.sprinId == sprintId && issue.type == issueType; });
    return issues;
}

